import axios from "axios";
import React from "react";
import { Link } from "react-router-dom";
// reactstrap components
import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
} from "reactstrap";
import { isThisTypeNode } from "typescript";
import { LOGIN_URL } from "variables/URL";
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      body: {
        phone: "",
        password: "",
      },
      ResponseError: "",
    };
  }
  toggleModal = (e) => {
    e.preventDefault();
    this.setState({ isOpen: !this.state.isOpen });
  };
  CheckLogin = () => {
    // this.Redirect();
    console.log(this.state.body);
    axios({
      method: "post",
      url: LOGIN_URL,
      data: this.state.body,
    }).then((res) => {
      debugger;
      if (res.data.httpstatus === 200) {
        if (res.data.response.Roleid === 3) {
          localStorage.setItem("IsLogined", true);
          localStorage.setItem("Name", res.data.response.UserName);
          localStorage.setItem("Email", res.data.response.Phone);
          this.Redirect();
        } else {
          window.alert("Your User Role is restricted");
        }
      } else {
        this.setState({ ResponseError: res.data.message });
      }
    });
  };
  Redirect = () => {
    let path = "/admin/index";
    this.props.history.push(path);
  };

  onChange = (state, val) => {
    this.setState((prevState) => ({
      body: {
        ...prevState.body,
        [state]: val,
      },
    }));
  };
  render() {
    return (
      <>
        <Col lg="5" md="7">
          <Card className="bg-secondary">
            <CardBody className="px-lg-5 py-lg-5">
              {/* <div className="text-center text-muted mb-4">
                <small>Or sign in with credentials</small>
              </div> */}
              <Form role="form">
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-email-83" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Email"
                      type="email"
                      autoComplete="new-email"
                      value={this.state.body.phone}
                      onChange={(e) => this.onChange("phone", e.target.value)}
                    />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Password"
                      type="password"
                      autoComplete="new-password"
                      value={this.state.body.password}
                      onChange={(e) =>
                        this.onChange("password", e.target.value)
                      }
                    />
                  </InputGroup>
                </FormGroup>
                <div className="custom-control custom-control-alternative custom-checkbox">
                  <input
                    className="custom-control-input"
                    id=" customCheckLogin"
                    type="checkbox"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor=" customCheckLogin"
                  >
                    <span className="text-muted">Remember me</span>
                  </label>
                </div>
                <div className="text-center">
                  <Button
                    className="my-4"
                    color="success"
                    type="button"
                    onClick={() => this.CheckLogin()}
                  >
                    Sign in
                  </Button>
                  <br />
                  {this.state.ResponseError !== "" &&
                    this.state.ResponseError !== undefined && (
                      <small className="text-danger">
                        <span>
                          {" "}
                          <i className="fas fa-exclamation-circle"></i>{" "}
                        </span>
                        {this.state.ResponseError}
                      </small>
                    )}
                </div>
              </Form>
            </CardBody>
          </Card>
          {/* <Row className="mt-3">
            <Col xs="12" className="text-center">
              <a
                className="text-light"
                onClick={(e) => this.toggleModal(e)}
                tag={Link}
              >
                <small>Forgot password?</small>
              </a>
            </Col>
          </Row> */}
        </Col>
      </>
    );
  }
}

export default Login;
