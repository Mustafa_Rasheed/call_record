import React, { useEffect, useState } from "react";
// reactstrap components
import {
  Badge,
  Card,
  CardHeader,
  Table,
  Container,
  Row,
  CardBody,
  Col,
  FormGroup,
  Label,
  Input,
  UncontrolledTooltip,
  Button,
} from "reactstrap";
import {
  showalluser,
  GET_AGENTS,
  RECORDING_BASE_URL,
  GetcallrecordsData,
} from "variables/URL";
import axios from "axios";
import { useHistory } from "react-router-dom";
import AudioPlayer from "react-h5-audio-player";
import "react-h5-audio-player/lib/styles.css";
// core components
import Header from "components/Headers/Header.js";
import { contains } from "validate.js";
export const DashBoard = () => {
  const history = useHistory();
  let options = [
    {
      key: 1,
      value: 1,
      title: "Name",
    },
    {
      key: 2,
      value: 2,
      title: "Name1",
    },
    {
      key: 3,
      value: 3,
      title: "Name2",
    },
    {
      key: 4,
      value: 4,
      title: "Name3",
    },
  ];

  const [body, setBody] = useState({
    CallDateTime: null,
    IncomingNumnber: null,
    showalluser,
  });

  const [data, setData] = useState(null);
  const [agent, setAgent] = useState(null);
  const [UserCallrecord, SetUserCallRecord] = useState(null);

  useEffect(() => {
    FetchData();
    FetchUserCallRecordData();
  }, [true]);

  const FetchUserCallRecordData = () => {
    console.log("*****", GetcallrecordsData);
    axios({
      method: "get",
      url: GetcallrecordsData,
    }).then((res) => {
      if (res.data.status === "true") {
        SetUserCallRecord(res.data.response);
      } else {
        console.log("Error: ");
      }
    });
  };

  const FetchData = () => {
    // this.Redirect();
    console.log(body);
    axios({
      method: "get",
      url: showalluser,
      // data: body,
    }).then((res) => {
      if (res.data.status === "true") {
        setAgent(res.data.response);
      } else {
        console.log("Error: ");
      }
    });
  };
  const Redirect = () => {
    let path = "/auth/login";
    history.push(path);
  };
  const FetchAgent = () => {
    // this.Redirect();
    console.log(body);
    axios({
      method: "post",
      url: GET_AGENTS,
      data: null,
    }).then((res) => {
      if (res.data.status === true) {
        console.log(res.data.data);
        setAgent(res.data.data);
        // console.log(data);
      } else {
        this.setState({ ResponseError: res.data.message });
      }
    });
  };
  const onChange = (val, state) => {
    setBody({ ...body, [state]: val });
    console.log(body);
  };

  useEffect(() => {
    if (
      localStorage.getItem("Name") === null ||
      localStorage.getItem("Email") === null ||
      localStorage.getItem("IsLogined") === null
    ) {
      Redirect();
    } else {
      console.log(localStorage.getItem("Name"));
      FetchData();
      FetchAgent();
    }
  }, [true]);
  useEffect(() => {
    if (body.IncomingNumnber === "Select Agent Name") {
      ResetData();
    } else {
      FetchData();
    }
  }, [body]);
  const [selected, setselected] = useState(1);
  const ResetData = () => {
    setBody({
      ...body,
      IncomingNumnber: null,
      OutGoingNumber: null,
      CallDateTime: null,
    });
  };
  const handleSelected = (selectedPage) => {
    setselected(selectedPage);
  };
  return (
    <>
      <Header />
      <Container className="mt--7" fluid>
        {" "}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="bg-transparent">
                <h3 className="mb-0">DashBoard</h3>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col lg="4" md="4" sm="12" xs="12">
                    <FormGroup>
                      <label
                        className="form-control-label"
                        htmlFor="input-agent"
                      >
                        Agents
                      </label>
                      <Input
                        className="form-control-alternative"
                        defaultValue={null}
                        id="input-agent"
                        placeholder="Select Aagent Nname"
                        type="select"
                      >
                        <option key={0} value={null}>
                          Select Agent Name
                        </option>

                        {agent !== null &&
                          agent.map((opt) => {
                            return (
                              <option value={opt.Userid} key={opt.Userid}>
                                {opt.UserName}
                              </option>
                            );
                          })}
                      </Input>
                    </FormGroup>
                  </Col>

                  <Col lg="4" md="4" sm="12" xs="12">
                    <FormGroup>
                      <label
                        className="form-control-label"
                        htmlFor="input-agent"
                      >
                        Enter Client Number
                      </label>
                      <Input
                        //type="number"
                        className="form-control-alternative"
                        id="input-agent"
                        placeholder="Enter Client Number"
                        defaultValue={null}
                        value={
                          body.OutGoingNumber !== null
                            ? body.OutGoingNumber
                            : ""
                        }
                        onChange={(e) =>
                          onChange(e.target.value, "OutGoingNumber")
                        }
                        type="text"
                      >
                        {/* {options.map((option) => {
                          return (
                            <option key={option.key} value={option.value}>
                              {option.title}
                            </option>
                          );
                        })} */}
                      </Input>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Button
                      type="button"
                      color="success"
                      onClick={() => {
                        FetchData();
                      }}
                    >
                      {" "}
                      Search
                    </Button>
                    <Button
                      type="button"
                      color="warning"
                      onClick={() => {
                        ResetData();
                      }}
                    >
                      {" "}
                      Reset
                    </Button>
                  </Col>
                  <Col></Col>
                </Row>
                <br />
                <br />
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">S.No</th>

                      <th scope="col">Agent Name</th>
                      {/* <th scope="col">Client Name</th> */}
                      <th scope="col">Client Number</th>
                      
                      <th scope="col">Call Type</th>
                      

                      {/* <th scope="col">Call Duration</th> */}
                      <th scope="col">Call Date</th>
                      <th scope="col" />
                    </tr>
                  </thead>
                  <tbody>
                  {UserCallrecord !== null &&
                          UserCallrecord.map((opt) => {
                            return (
                             <tr>
                               <td>{opt.Callid}</td>
                               <td>{opt.UserName}</td>

                               <td>{opt.Phone}</td>
                               <td>{opt.Typename}</td>
                               <td>{opt.DateTime}</td>
                            
                             </tr>
                            );
                          })}

                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};
