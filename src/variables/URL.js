// export const BASE_URL = "http://kgcp.pk/CRMRecordingAPI/api";

export const BASE_URL = "http://192.168.18.121:8001/api";

// export const LOGIN_URL = `${BASE_URL}/Login`;
// export const GET_RECORDINGS = `${BASE_URL}/Recording/GetRecording`;
// export const GET_AGENTS = `${BASE_URL}/Agent/GetAgents`;

export const LOGIN_URL = `${BASE_URL}/userlogin`;
export const showalluser = `${BASE_URL}/showalluser`;
export const GET_AGENTS = `${BASE_URL}/Agent/GetAgents`;
export const GetcallrecordsData =`${BASE_URL}/GetcallrecordsData`;

// export const RECORDING_BASE_URL = `http://kgs.pointhook.com/assets/uploads/convert/`;

export const RECORDING_BASE_URL = `http://squarepro.net/callrecordings/`;
